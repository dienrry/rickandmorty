//
//  PersonajesPrincipalViewController.swift
//  RickAndMorty
//
//  Created by Diego Miranda [GDL] on 8/29/19.
//  Copyright © 2019 Diego Miranda [GDL]. All rights reserved.
//

import UIKit
import Foundation

class PersonajesPrincipalViewController: UIViewController, Toast, Networking {
    
    //MARK: - Variables
    @IBOutlet weak var tvwPersonajes: UITableView!
    @IBOutlet weak var vwEspera: UIView!
    @IBOutlet weak var avCargando: UIActivityIndicatorView!
    var listPersonajes: NSMutableArray = []
    var oWebServices = cWebServices()
    var delegate:AvatarViewCellDelegate?
    var listaOrigen:String?
    

    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title="Rick And Morty"
        
        self.tvwPersonajes.register(UINib(nibName: "PersonajesTableViewCell", bundle: nil), forCellReuseIdentifier: "PersonajesTableViewCell")
        self.tvwPersonajes.register(UINib(nibName: "AvatarsTableViewCell", bundle: nil), forCellReuseIdentifier: "AvatarsTableViewCell")
        
        self.tvwPersonajes.tableFooterView = UIView(frame: CGRect(x:0,y:0,width:0,height:0))
        obtenerPersonajes(iPagina: Int.random(in: 1..<15))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        tvwPersonajes.reloadData()
    }
    
    
    //MARK: -Funciones
    func obtenerPersonajes(iPagina:Int){
        if isConected(){
            vwEspera.isHidden = false
            avCargando.isHidden = false
            avCargando.startAnimating()
            listPersonajes=[]

            let oJSON:JSON? = self.oWebServices.obtenerPersonajes(iPagina: iPagina)
            DispatchQueue.global().async {
                
                guard (UserDefaults.standard.string(forKey: "ValidacionHTTP") != "ConeCaida")
                    else{
                        //habilita la interaccion con las pantallas
                        self.tvwPersonajes.reloadData()
                        if self.avCargando.isAnimating {
                            self.avCargando.stopAnimating()
                            self.avCargando.isHidden = true
                            self.vwEspera.isHidden  = true
                            self.Toast(viewController: self, mensaje: "Error de Conexión")
                        }
                        return
                }

                for (_, subJson): (String, JSON) in oJSON!["results"] {
                    var oPersonajes:jPersonajes=jPersonajes()
                    oPersonajes.id = subJson["id"].intValue
                    oPersonajes.nombre = subJson["name"].stringValue
                    oPersonajes.estatus = subJson["status"].stringValue
                    oPersonajes.especie = subJson["species"].stringValue
                    oPersonajes.origen = subJson["origin"]["name"].stringValue
                    oPersonajes.genero = subJson["gender"].stringValue
                    oPersonajes.localizacion = subJson["location"]["name"].stringValue
                    oPersonajes.fotografia =  NSURL(string: subJson["image"].stringValue) as URL?
  
                    self.listPersonajes.add(oPersonajes)
                }

                DispatchQueue.main.async {
                    self.tvwPersonajes.reloadData()
                    if self.avCargando.isAnimating {
                        self.avCargando.stopAnimating()
                        self.avCargando.isHidden = true
                        self.vwEspera.isHidden  = true
                    }
                }
            }
        }
        else{
            Toast(viewController: self, mensaje: "Conexión Perdida")
        }
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let PersonajesDetalle:PersonajesDetalleViewController = segue.destination as! PersonajesDetalleViewController
        
        if listaOrigen=="Avatar"{
            PersonajesDetalle.oDatosPersonaje = sender as? jPersonajes
        }else{
            let indexPath = sender
            PersonajesDetalle.oDatosPersonaje = listPersonajes.object(at: indexPath as! Int) as? jPersonajes
        }
    }
   
}//Class PersonajesPrincipalViewController

extension PersonajesPrincipalViewController:UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 130
        }else{
           return 60
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return listPersonajes.count
        }
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarsTableViewCell", for: indexPath)as! AvatarsTableViewCell
            cell.AvatarDelegate=self
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonajesTableViewCell", for: indexPath) as! PersonajesTableViewCell
            
            let oPersonajes:jPersonajes = listPersonajes.object(at: indexPath.row) as! jPersonajes
            cell.lblNombrePersonaje.text = oPersonajes.nombre
            cell.layer.borderWidth=1.0
            cell.layer.borderColor=#colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "SegueDetallePersonaje", sender: indexPath.row)
    }


}

extension PersonajesPrincipalViewController:AvatarViewCellDelegate{
    func didTapAvatar(sender: Any) {
        self.listaOrigen="Avatar"
        self.performSegue(withIdentifier: "SegueDetallePersonaje", sender: sender)
        self.listaOrigen=""
    }
    
}
