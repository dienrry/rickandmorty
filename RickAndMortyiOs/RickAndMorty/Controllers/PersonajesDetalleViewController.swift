//
//  PersonajesDetalleViewController.swift
//  RickAndMorty
//
//  Created by Diego Miranda [GDL] on 8/29/19.
//  Copyright © 2019 Diego Miranda [GDL]. All rights reserved.
//

import UIKit

class PersonajesDetalleViewController: UIViewController {
    
    
    @IBOutlet weak var imgPersonaje: UIImageView!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblEstatus: UILabel!
    @IBOutlet weak var lblEspecie: UILabel!
    @IBOutlet weak var lblGenero: UILabel!
    @IBOutlet weak var lblOrigen: UILabel!
    @IBOutlet weak var lblLocalizacion: UILabel!
    
    var oDatosPersonaje:jPersonajes?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        mostrarDatos()
    }
    
    
    // MARK: - Funciones    
    func mostrarDatos(){
        lblNombre.text = oDatosPersonaje?.nombre
        lblEstatus.text = oDatosPersonaje?.estatus
        lblEspecie.text = oDatosPersonaje?.especie
        lblGenero.text = oDatosPersonaje?.genero
        lblOrigen.text = oDatosPersonaje?.origen
        lblLocalizacion.text = oDatosPersonaje?.localizacion
        let imageData = try! Data(contentsOf: oDatosPersonaje!.fotografia!)
        imgPersonaje.image = UIImage(data: imageData)
    }
    
    func config(){
        //Oculta el texto del backbutton
        self.navigationController!.navigationBar.topItem!.title = ""
        imgPersonaje.layer.cornerRadius=80
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
}
