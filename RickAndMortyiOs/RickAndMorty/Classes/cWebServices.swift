//
//  cWebServices.swift
//  RickAndMorty
//
//  Created by Diego Miranda [GDL] on 8/29/19.
//  Copyright © 2019 Diego Miranda [GDL]. All rights reserved.
//

import Foundation

class cWebServices {
   
    let sURL = "https://rickandmortyapi.com/api/character?page="
    
    func obtenerPersonajes(iPagina:Int) -> JSON?{
        let url: URL = URL(string: sURL + String(iPagina))!
        let oJSON = HandlerRequest(metodo: .Get, url: url, body: nil, refresh: nil)
        return oJSON
    }
    
}
