//
//  cUtilidades.swift
//  Paises
//
//  Created by Diego Miranda on 8/27/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

    //MARK: - Toast
    protocol Toast {
        func Toast(viewController:UIViewController, mensaje:String)
    }

    extension Toast{
        func Toast(viewController:UIViewController, mensaje:String){
            viewController.view?.makeToast(mensaje, duration: 1.5, position: .bottom)
        }
        
        func MostrarActivity(viewcontroller:UIViewController){
            viewcontroller.view?.makeToastActivity(.center)
        }
        func OcultarActivity(viewcontroller:UIViewController){
            viewcontroller.view?.hideToastActivity()
        }
    }


//MARK: - Conectividad

    //Protocolo para saber el estado de la red
    protocol Networking {
//        func updateUserInterface()
//        func statusManager(_ notification: NSNotification)
    }

    extension Networking{

        func isConected() -> Bool{
            guard let status = Network.reachability?.status else { return false }
            switch status{
            case .unreachable:
                return false
            default:
                return true
            }
        }
}


