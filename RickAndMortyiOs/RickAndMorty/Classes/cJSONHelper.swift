//
//  cJSONHelper.swift
//  Paises
//
//  Created by Diego Miranda [GDL] on 8/28/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import Foundation
import UIKit

//MARK: - URL Session Extension
extension URLSession {
    
    
    func synchronousDataTask(with request:URLRequest) -> ([Data?]) {
        let semaphore = DispatchSemaphore(value: 0)
        var _dat : [Data?] = []
        var _res : URLResponse?
        var _err : Error?
        
        self.dataTask(with: request) { dat, res, err in
            _dat.append(dat)
            _res = res
            _err = err
            
            if let error = _err?.localizedDescription{
                print(error)
            }
            
            let response = _res as? HTTPURLResponse
            var codigo:Int
            
            if response?.statusCode==nil{
                codigo=0
            }else{
                codigo = response!.statusCode
            }
            
            if  (codigo >= 200 && codigo <= 299){
                UserDefaults.standard.setValue("OK", forKey: "ValidacionHTTP")
            }else if (codigo >= 401 && codigo <= 499){
                UserDefaults.standard.setValue("NoAutorizado", forKey:"ValidacionHTTP")
            }else if (codigo >= 500 && codigo <= 599 || codigo == 400){
                UserDefaults.standard.setValue("ErrorServer", forKey: "ValidacionHTTP")
            }
            else if(codigo==0){
                UserDefaults.standard.setValue("ConeCaida", forKey: "ValidacionHTTP")
                
            }
            
           // print("Codigo HTTP",codigo)
            
            //Añadimos los tokens
            _dat.append( (response?.allHeaderFields["Autorization"] as? String)?.data(using: String.Encoding.utf8))
            
            semaphore.signal()
            }.resume()
        _ = semaphore.wait(timeout: .distantFuture)
        return (_dat)
    }
}


//MARK: - Casos de Verbos HTTP
//Verbos HTTP a usar
enum verboHTTP:String{
    case Post = "POST"
    case Put = "PUT"
    case Get = "GET"
    case Delete = "DELETE"
}


//MARK: - Request y Handler
func jsonRequest(metodo:verboHTTP, url: URL, body: String?, refresh:String?) -> URLRequest{
    let bodyData = body?.data(using: String.Encoding.utf8, allowLossyConversion: true)
    let bodyType: String = "application/json"
    let bodyLength: String = bodyData != nil ? String (bodyData!.count) : "0"
    var request = URLRequest(url: url)
    var _: HTTPURLResponse?
    _ = UIApplication.shared.delegate as! AppDelegate
    
    /**/ request.timeoutInterval=180.0
    request.httpMethod = metodo.rawValue
    request.setValue(bodyType, forHTTPHeaderField: "Content-Type")
    request.setValue(bodyLength, forHTTPHeaderField: "Content-Length")
    
    // Validaciones de los Token
    if refresh != nil{
        if let refreshToken = refresh{
            request.setValue(refreshToken, forHTTPHeaderField: "Authorization")
        }
    }else{
        if let Token = UserDefaults.standard.string(forKey: "Access_token"){
            request.setValue(Token, forHTTPHeaderField: "Authorization")
        }
    }
    if (bodyData != nil  && !bodyData!.isEmpty){
        request.httpBody = bodyData
    }
    
    
    return request
}

func HandlerRequest(metodo: verboHTTP, url: URL, body: String?, refresh: String?) -> JSON?{
    let config = URLSessionConfiguration.default
    let session = URLSession(configuration: config)
    _ = cWebServices()
    let _:HTTPURLResponse?
    _ = UIApplication.shared.delegate as! AppDelegate
    // var httpResponse = response as?HTTPURLResponse
    let request = jsonRequest(metodo: metodo, url: url, body: body, refresh: refresh)
    
    var oJSON: JSON?
    
    let data = (data: session.synchronousDataTask(with: request), options:JSONSerialization.ReadingOptions(rawValue: String.Encoding.utf8.rawValue))
    guard let json = data.0[0] else {return nil}
    oJSON = JSON(data: json)
    
    return oJSON!
}

