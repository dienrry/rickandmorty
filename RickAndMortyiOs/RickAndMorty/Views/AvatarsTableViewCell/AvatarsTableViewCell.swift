//
//  AvatarsTableViewCell.swift
//  RickAndMorty
//
//  Created by Diego Miranda [GDL] on 8/29/19.
//  Copyright © 2019 Diego Miranda [GDL]. All rights reserved.
//

import UIKit
import Foundation

protocol AvatarViewCellDelegate {
    func didTapAvatar(sender:Any)
}

class AvatarsTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,Toast,Networking {
    
    //MARK: - Variables
    @IBOutlet weak var cvwAvatars: UICollectionView!
    var avatar:jPersonajes?
    var listPersonajesAvatars: NSMutableArray = []
    let cellReuseId = "PersonajesCollectionViewCell"
    let oWebServices = cWebServices()
    
    var AvatarDelegate:AvatarViewCellDelegate?
    
    class var customCell: AvatarsTableViewCell{
        let cell = Bundle.main.loadNibNamed("AvatarsTableViewCell", owner: self, options: nil)?.last
        return cell as! AvatarsTableViewCell
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        //Diseño al CollectionCell
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 120, height: 120)
        flowLayout.minimumLineSpacing = 2.0
        flowLayout.minimumInteritemSpacing = 5.0
        self.cvwAvatars.collectionViewLayout = flowLayout
        self.cvwAvatars.dataSource = self
        self.cvwAvatars.delegate = self
        
        //register the xib for collection view cell
        let cellNib = UINib(nibName: "PersonajesCollectionViewCell", bundle: nil)
        self.cvwAvatars.register(cellNib, forCellWithReuseIdentifier:cellReuseId )
        
        obtenerPersonajes(iPagina:Int.random(in: 1..<15))

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    
    //MARK: - Collection View
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let oPersonajes:jPersonajes = listPersonajesAvatars.object(at: indexPath.row) as! jPersonajes
        AvatarDelegate?.didTapAvatar(sender: oPersonajes)
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return listPersonajesAvatars.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseId, for: indexPath) as? PersonajesCollectionViewCell

        let oPersonajes:jPersonajes = listPersonajesAvatars.object(at: indexPath.row) as! jPersonajes
        
        cell?.lblNombre.text = oPersonajes.nombre
        let imageData = try! Data(contentsOf: oPersonajes.fotografia!)
        cell?.imgAvatar.image = UIImage(data: imageData)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    
    func obtenerPersonajes(iPagina:Int){
        if isConected(){
            listPersonajesAvatars=[]
            let oJSON:JSON? = self.oWebServices.obtenerPersonajes(iPagina: iPagina)
            DispatchQueue.global().async {
      
                for (_, subJson): (String, JSON) in oJSON!["results"] {
                    var oPersonajes:jPersonajes=jPersonajes()
                    
                    oPersonajes.id = subJson["id"].intValue
                    oPersonajes.nombre = subJson["name"].stringValue
                    oPersonajes.estatus = subJson["status"].stringValue
                    oPersonajes.especie = subJson["species"].stringValue
                    oPersonajes.origen = subJson["origin"]["name"].stringValue
                    oPersonajes.genero = subJson["gender"].stringValue
                    oPersonajes.localizacion = subJson["location"]["name"].stringValue
                    oPersonajes.fotografia =  NSURL(string: subJson["image"].stringValue) as URL?
                    
                    self.listPersonajesAvatars.add(oPersonajes)
                }
                
                DispatchQueue.main.async {
                    self.cvwAvatars.reloadData()
                }
            }
        }
    }
}


