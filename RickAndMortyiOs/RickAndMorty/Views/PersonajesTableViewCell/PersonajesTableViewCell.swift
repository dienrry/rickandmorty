//
//  PersonajesTableViewCell.swift
//  RickAndMorty
//
//  Created by Diego Miranda [GDL] on 8/29/19.
//  Copyright © 2019 Diego Miranda [GDL]. All rights reserved.
//

import UIKit

class PersonajesTableViewCell: UITableViewCell {
    
    //MARK: - Variables
    @IBOutlet weak var lblNombrePersonaje: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
