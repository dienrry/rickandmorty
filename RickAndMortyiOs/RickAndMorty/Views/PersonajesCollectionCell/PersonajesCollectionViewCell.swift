//
//  PersonajesCollectionViewCell.swift
//  RickAndMorty
//
//  Created by Diego Miranda [GDL] on 8/29/19.
//  Copyright © 2019 Diego Miranda [GDL]. All rights reserved.
//

import UIKit

class PersonajesCollectionViewCell: UICollectionViewCell {
    
    
    //MARK: - Variables
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblNombre: UILabel!
    
    class var CustomCell : PersonajesCollectionViewCell {
        let cell = Bundle.main.loadNibNamed("PersonajesCollectionViewCell", owner: self, options: nil)?.last
        return cell as! PersonajesCollectionViewCell
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgAvatar.layer.cornerRadius = 45
        
    }
    
    func updateCellWithImage(nombre:String,imagen:URL) {
        lblNombre.text =  nombre
        let data = try? Data(contentsOf: imagen)
        if let imageData = data {
            imgAvatar.image = UIImage(data: imageData)
        }
    }

}
