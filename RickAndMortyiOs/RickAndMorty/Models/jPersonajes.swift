//
//  jPersonajes.swift
//  RickAndMorty
//
//  Created by Diego Miranda [GDL] on 8/29/19.
//  Copyright © 2019 Diego Miranda [GDL]. All rights reserved.
//

import Foundation

struct jPersonajes {
    
    var id:Int?
    var nombre:String?
    var estatus:String?
    var origen:String?
    var genero:String?
    var especie:String?
    var localizacion:String?
    var fotografia:URL?
}

